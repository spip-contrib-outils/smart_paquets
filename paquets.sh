#!/bin/sh

cd /home/smart_paquets
export LC_ALL=fr_FR@euro
export LC_CTYPE=en_US.UTF-8


if [ $# -eq 1 ]
then

	if [ $1 == "spip" ]|| [ $1 == "tout" ]
	then
		# Fabrication des archives de SPIP
		/usr/bin/php empaqueteur.php svn://trac.rezo.net/spip spip paquets archivelist.txt archives plugin svn spip-dev@rezo.net paquets-spip@rezo.net
	fi

	if [ $1 == "spip-zone" ]|| [ $1 == "tout" ]
	then
		# Fabrication des archives des plugins principaux (plugins n'appartenant pas au core et compatibles avec une version SPIP encore maintenue)
		/usr/bin/php empaqueteur.php svn://zone.spip.org/spip-zone spip-zone paquets archivelist.txt archives paquet gitsvn spip-dev@rezo.net paquets-zone@rezo.net
	fi

	if [ $1 == "contribs" ]|| [ $1 == "tout" ]
	then
		# Fabrication des archives des contributions qui ne sont pas des plugins
		/usr/bin/php empaqueteur.php svn://zone.spip.org/spip-zone spip-zone contribs archivelist_autres.txt archives_autres paquet gitsvn spip-dev@rezo.net paquets-zone@rezo.net
	fi

	if [ $1 == "core" ]|| [ $1 == "tout" ]
	then
		# Fabrication des archives des plugins du core
		/usr/bin/php empaqueteur.php svn://zone.spip.org/spip-zone spip-zone core archivelist_core.txt archives_core paquet gitsvn spip-dev@rezo.net paquets-zone@rezo.net
	fi

	if [ $1 == "externals" ]|| [ $1 == "tout" ]
	then
		# Fabrication des archives des plugins externes (github, autre svn) references via un _externals_
		/usr/bin/php empaqueteur.php svn://zone.spip.org/spip-zone/_externals_ spip-zone-externals externals archivelist_externals.txt archives_externals paquet svn spip-dev@rezo.net paquets-zone@rezo.net
	fi


	if [ $1 == "grenier" ]|| [ $1 == "tout" ]
	then
		# Fabrication des veux plugins archives
		/usr/bin/php empaqueteur.php svn://zone.spip.org/spip-zone spip-zone grenier archivelist_grenier.txt archives_grenier paquet gitsvn spip-dev@rezo.net paquets-zone@rezo.net
	fi


else
	echo "Utilisation : $0 [tout|spip|spip-zone|contribs|core|externals|grenier]"
fi
