# empaqueteur

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

#toutes les heures:04 => les plugins principaux de SPIP-ZONE
04 * * * *   root   /home/smart_paquets/paquets.sh spip-zone 2>&1 > /home/smart_paquets/paquets/paquets.log.txt

#toutes les heures:34 => SPIP
#pour regenerer spip  : touch /home/smart_paquets/spip/archivelist.txt
34 * * * *   root   /home/smart_paquets/paquets.sh spip 2>&1 > /home/smart_paquets/paquets/paquets-spip.log.txt

#tous les jours a 3h49 du matin => les contributions non plugins
49 03 * * *   root   /home/smart_paquets/paquets.sh contribs 2>&1 > /home/smart_paquets/paquets/contribs.log.txt

#tous les jours a 2h49 du matin => les plugins du core
49 02 * * *   root   /home/smart_paquets/paquets core 2>&1 > /home/smart_paquets/paquets/core.log.txt

#tous les jours a 1h49 du matin => les plugins du grenier
49 01 * * *   root   /home/smart_paquets/paquets.sh grenier 2>&1 > /home/smart_paquets/paquets/grenier.log.txt

#toutes les heures:19 => les plugins externes (Github) uniquement si archivelist_externals.txt est modifie
19 * * * *   root   /home/smart_paquets/paquets.sh externals 2>&1 > /home/smart_paquets/paquets/externals.log.txt