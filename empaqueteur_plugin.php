<?php

// Renvoie le path complet du logo a partir de la balise icon de plugin.xml et de la racine des sources
function empaqueteur_plugin_logo($plugin_xml, $dir_source) {
  return !preg_match('#<icon[^>]*>\s*(.+)\s*</icon>#i', $plugin_xml, $matches) ? '' : ($dir_source . '/' . trim($matches[1]));
}

?>
